# Mutual information

Estimates the mutual information between a collection of time series and a list of labels, where each label is associated with one set of time series and all labels are equally likely.